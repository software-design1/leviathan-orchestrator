export interface LeviathanConfiguration {
  name: string;
  basePath?: string;
  controllers: LeviathanController[];
}

export interface LeviathanController {
  name: string;
  path: string;
  mem?: Record<string, any>;
  type: LeviathanApiCallType;
  callList?: LeviathanApiCall[];
  aggregator?: LevithanAggregator;
}

export interface LevithanAggregator {
  aggregate(data: LeviathanControllerMemory): void;
}

export interface LeviathanApiCall {
  type: LeviathanApiCallType;
  async?: boolean;
  url: string | ((data: LeviathanControllerMemory) => string);
  data?: any | ((data: LeviathanControllerMemory) => any);
  header?: any | ((data: LeviathanControllerMemory) => any);
}

export enum LeviathanApiCallType {
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT',
  PATCH = 'PATCH',
  DELETE = 'DELETE',
}

export interface LeviathanControllerMemory {
  data: any[];
  reqBody?: any;
  reqQuery?: any;
  reqHeader?: any;
  res?: any;
}
