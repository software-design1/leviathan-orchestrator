import { Injectable } from '@nestjs/common';
import * as http from 'http';
import * as https from 'https';
import { LeviathanLoggerService } from './leviathan-logger.service';
import { URL } from 'url';

@Injectable()
export class LeviathanHttpService {
  constructor(private leviathanLoggerService: LeviathanLoggerService) {}

  async request(
    urlString: string,
    option: http.RequestOptions,
    body?: any,
  ): Promise<any> {
    this.leviathanLoggerService.debug(`Leviathan request on: ${urlString}`);

    const url = new URL(urlString);
    const clientHttp = url.protocol == 'http' ? http : https;

    return new Promise<any>((resolve, reject) => {
      let req = clientHttp.request(
        {
          ...option,
          hostname: url.hostname,
          path: url.pathname,
        },
        (res) => {
          let resData = '';

          res.on('data', (data: Buffer) => {
            resData += data.toString();
          });

          res.on('end', () => {
            resolve(JSON.parse(resData));
          });
        },
      );

      req.on('error', (error) => reject(error));
      if (body !== undefined) {
        req.write(body);
      }
      req.end();
    });
  }
}
