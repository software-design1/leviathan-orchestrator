import { Injectable, Logger } from "@nestjs/common";

@Injectable()
export class LeviathanLoggerService {
  private readonly logger = new Logger(LeviathanLoggerService.name)

  log(message: string) : void {
    this.logger.log(message)
  }

  error(message: string) : void {
    this.logger.error(message)
  }

  debug(message: string) : void {
    this.logger.debug(message)
  }

  warn(message: string) : void {
    this.logger.warn(message)
  }
}
